<?php

function countSumFormHugeStrings($stringOne, $stringTwo) {
    $sum = gmp_init($stringOne) + gmp_init($stringTwo);
    return $sum;
}

$stringOne = '10000000000000000000000001';
$stringTwo = '10000000000000000000000003';

echo countSumFormHugeStrings($stringOne, $stringTwo);